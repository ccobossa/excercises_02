using System;

namespace Exercises_02
{
    public class Bus : Vehicle
    {
        static protected string _color = "White";
        protected int _capacity;

        public int Get__capacity()
        {
            return _capacity;
        }
        public void Set__capacity(int capacity)
        {
            _capacity = capacity;
            fare();
        }
        static string Get__color()
        {
            return _color;
        }
        static public void Set__color(string color)
        {
            _color = color;
        }
  
        public String Print()
        {
            string s;

            s = "Speed: " + Get__maxSpeed() 
                + " " + "Mileage: " + Get__mileage()
                + " " + "Capacity: " + Get__capacity()
                + " " + "Color: " + _color
                + " " + "Cost: " + Get__cost();

            return s;
        }
        public void fare()
        {
            _cost = _capacity * 100;
        }

        public Bus(float speed, float mileage)
        {
            _maxSpeed = speed;
            _mileage = mileage;
            _capacity = 0;
            fare();
        }
        public Bus(float speed, float mileage, int capacity)
        {
            _maxSpeed = speed;
            _mileage = mileage;
            _capacity = capacity;
            fare();
        }
    }
}