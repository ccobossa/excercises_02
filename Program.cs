﻿using System;

namespace Exercises_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Bus bus0 = new Bus(10f, 20f);
            Console.WriteLine(bus0.Print());
            Bus bus1 = new Bus(5f, 10f, 50);
            Console.WriteLine(bus1.Print());

            bus0.Set__capacity(20);
            Console.WriteLine(bus0.Print());
            
            Bus.Set__color("Red");

            Console.WriteLine("------------------------");
            Console.WriteLine(bus0.Print());
            Console.WriteLine(bus1.Print());

            bus0.Set__capacity(100);
            Console.WriteLine("------------------------");
            Console.WriteLine("Cost of Vehicle: " 
                + new Vehicle(3f,30f).Get__cost());
            Console.WriteLine(bus0.Print());
            Console.WriteLine(bus1.Print());
            //Console.WriteLine(bus0.GetType().ToString());
            string s = bus0.GetType().ToString();
            if(s.Equals( "Exercises_02.Bus"))
            {
                Console.WriteLine("Bus0 is bus?: YES");
            }
            else
            {
                Console.WriteLine("Bus0 is bus?: NO");
            }
            
            if(bus0.GetType().IsInstanceOfType(new Vehicle()))
            {
                System.Console.WriteLine("Es hijo");
            }
            else
            {
                System.Console.WriteLine("No es hijo");
            }
        }
    }
}
