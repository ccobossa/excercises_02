using System;

namespace Exercises_02
{
    public class Vehicle : EmptyVehicle
    {
        protected float _maxSpeed;
        protected float _mileage;
        protected int _cost;

        public float Get__maxSpeed()
        {
            return _maxSpeed;
        }
        public void Set__maxSpeed(float speed)
        {
            this._maxSpeed = speed;
        }
        public float Get__mileage()
        {
            return _mileage;
        }
        public void Set__mileage(float mileage)
        {
            this._mileage = mileage;
        }
        public int Get__cost()
        {
            return _cost;
        }

        public void fare()
        {
            _cost = 34567;
        }

        public Vehicle()
        {
            _maxSpeed = 1f;
            _mileage = 1f;
            fare();
        }
        public Vehicle(float speed, float startingMileage)
        {
            _maxSpeed = speed;
            _mileage = startingMileage;
            fare();
        }
    }
}